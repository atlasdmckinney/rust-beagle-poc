$message = '1,ABCXYZ1000,-45.3'
$messageCount = 100000000000;
$port = new-Object System.IO.Ports.SerialPort COM11, 115200, None, 8, one;

Write-Host "Opening Com Port 5";
$port.open()
$isOpen = $port.isOpen;
Write-Host "Com port status: $isOpen";

Write-Host "Writing $messageCount messages";
for ($i = 0; $i -lt $messageCount; $i++) {
    Write-Host "$i of $messageCount";
    $port.Write($message);
}
$port.close();