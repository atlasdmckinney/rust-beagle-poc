//! Simple example that echoes received serial traffic to stdout
//! docker run --rm -it -v %cd%:/build/project build-test /bin/bash
extern crate mio;
extern crate mio_serial;
extern crate nmea;

#[cfg(unix)]
use mio::unix::UnixReady;
use mio::{Events, Poll, PollOpt, Ready, Token};
use nmea::Nmea;
// use std::env;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Read;
use std::io::Write;

const GPS_SERIAL_TOKEN: Token = Token(0);
const RFID_SERIAL_TOKEN: Token = Token(1);

#[cfg(unix)]
fn ready_of_interest() -> Ready {
    Ready::readable() | UnixReady::hup() | UnixReady::error()
}

#[cfg(unix)]
fn is_closed(state: Ready) -> bool {
    state.contains(UnixReady::hup() | UnixReady::error())
}

pub fn main() {
    // let mut args = env::args();
    let gps_tty_path = "/dev/ttyO4";
    let rfid_tty_path = "/dev/ttyO2";

    let poll = Poll::new().unwrap();
    let mut events = Events::with_capacity(1024);

    // Create the listener
    let mut settings = mio_serial::SerialPortSettings::default();
    settings.baud_rate = 115200;

    println!("Opening {} at 115200,8N1", gps_tty_path);
    let gps_rx = mio_serial::Serial::from_path(&gps_tty_path, &settings).unwrap();

    println!("Opening {} at 115200,8N1", rfid_tty_path);
    let mut rfid_rx = mio_serial::Serial::from_path(&rfid_tty_path, &settings).unwrap();

    println!("Waiting for reads");
    poll.register(
        &gps_rx,
        GPS_SERIAL_TOKEN,
        ready_of_interest(),
        PollOpt::edge(),
    )
    .unwrap();

    poll.register(
        &rfid_rx,
        RFID_SERIAL_TOKEN,
        ready_of_interest(),
        PollOpt::edge(),
    )
    .unwrap();

    let gps_rx_buf = [0u8; 2048];
    let mut rfid_rx_buf = [0u8; 2048];

    let mut gps_file = File::create("GPS_READS.txt").unwrap();
    let mut rfid_file = File::create("RFID_READS.txt").unwrap();

    let mut nmea = Nmea::new();

    let mut gps_reader = BufReader::new(&gps_rx);
    let mut gps_line = String::new();

    let reader_enable = "AT$GPSNMUN=3,1,0,0,0,0,1";
    let mut gps_writer = BufWriter::new(&gps_rx);

    println!("{:?}", reader_enable);
    gps_writer
        .write_all(reader_enable.as_bytes())
        .expect("COULD NOT WRITE TO GPS");

    let mut rolling = false;

    'outer: loop {
        if let Err(ref e) = poll.poll(&mut events, None) {
            println!("poll failed: {}", e);
            break;
        }

        if events.is_empty() {
            println!("Read timed out!");
            continue;
        }

        for event in events.iter() {
            match event.token() {
                GPS_SERIAL_TOKEN => {
                    let ready = event.readiness();
                    if is_closed(ready) {
                        println!("Quitting due to event: {:?}", ready);
                        break 'outer;
                    }
                    if ready.is_readable() {
                        // With edge triggered events, we must perform reading until we receive a WouldBlock.
                        // See https://docs.rs/mio/0.6/mio/struct.Poll.html for details.
                        loop {
                            match gps_reader.read_line(&mut gps_line) {
                                Ok(count) => {
                                    println!("{:?}", gps_line);
                                    nmea.parse(&gps_line).unwrap();
                                    // println!("{}", gps_line);
                                    // println!("{}", nmea);
                                    // println!("{:?}", nmea.speed_over_ground);
                                    gps_line = String::new();

                                    if !rolling
                                        && nmea.speed_over_ground.is_some()
                                        && nmea.speed_over_ground.unwrap() > 0.1
                                    {
                                        rolling = true;
                                        println!("Rolling");
                                    } else if rolling
                                        && nmea.speed_over_ground.is_some()
                                        && nmea.speed_over_ground.unwrap() <= 0.1
                                    {
                                        println!("Not Rolling");
                                        rolling = false;
                                    }
                                    gps_file
                                        .write_all(&gps_rx_buf[..count])
                                        .expect("file not written")
                                }
                                Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                                    break;
                                }
                                Err(ref e) => {
                                    println!("Quitting due to read error: {}", e);
                                    break 'outer;
                                }
                            }
                        }
                    }
                }
                RFID_SERIAL_TOKEN => {
                    let ready = event.readiness();
                    if is_closed(ready) {
                        println!("Quitting due to event: {:?}", ready);
                        break 'outer;
                    }
                    if ready.is_readable() {
                        // With edge triggered events, we must perform reading until we receive a WouldBlock.
                        // See https://docs.rs/mio/0.6/mio/struct.Poll.html for details.
                        loop {
                            if rolling {
                                match rfid_rx.read(&mut rfid_rx_buf) {
                                    Ok(count) => {
                                        // let line = String::from_utf8_lossy(&rfid_rx_buf[..count]);
                                        rfid_file
                                            .write_all(&rfid_rx_buf[..count])
                                            .expect("file not written");
                                    }
                                    Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                                        break;
                                    }
                                    Err(ref e) => {
                                        println!("Quitting due to read error: {}", e);
                                        break 'outer;
                                    }
                                }
                            }
                        }
                    }
                }
                t => unreachable!("Unexpected token: {:?}", t),
            }
        }
    }
}
